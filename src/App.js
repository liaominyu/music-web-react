import React, { memo } from 'react'
import {Provider} from 'react-redux'
import { HashRouter } from 'react-router-dom'

import store from './store'

import HYAppHeader from '@/components/app-header'
import HYAppFooter from '@/components/app-footer'
import AppPlayerBar from '@/pages/player/app-player-bar'
import RouterWrapper from '@/router'



export default memo(function App() {

  return (
    <Provider store={store}>
      <HashRouter>
        <HYAppHeader/>
        <RouterWrapper/>
        <AppPlayerBar/>
        <HYAppFooter/>
      </HashRouter>
    </Provider>
    
  )
})

import {combineReducers} from 'redux-immutable'

import {reducer as recommendsReducer } from '@/pages/discover/c-pages/recommend/store'
import {reducer as playerReducer} from '../pages/player/store'

const cReducer = combineReducers({
  recommend:recommendsReducer,
  player:playerReducer
});

export default cReducer;
import React, { memo, useEffect , useCallback, useRef,useState} from 'react'

import {useDispatch,useSelector,shallowEqual} from 'react-redux';
import { getTopBannerAction } from '../../store/actionCreators';

import { Carousel } from 'antd';

import {
  BannerWrapper,
  BannerControl,
  BannerLeft,
  BannerRight
} from './style';

export default memo(function RecommendTopBanner() {

  const [currentIndex, setCurrentIndex] = useState(0);
  

  //组件和redux关联：获取数据进行操作
  //第二参数默认进行===比较，使用shallowEqual进行浅层比较
  const {topBanners} = useSelector(state => ({
    // topBanners:state.get("recommend").get("topBanners")
    topBanners:state.getIn(["recommend","topBanners"])
  }),shallowEqual);

  const dispatch = useDispatch();


  const BanRef = useRef();
  useEffect(() => {
    dispatch(getTopBannerAction());
  }, [dispatch]);

  const bannerChange = useCallback((from , to)=>{
    setCurrentIndex(to)
  },[]);

  const bgImage = topBanners[currentIndex] && (topBanners[currentIndex].imageUrl+"?imageView&blur=40x20")
  return (
    <BannerWrapper bgImage={bgImage}>
      <div className="banner wrap-v2">
        <BannerLeft>
          <Carousel effect="fade" autoplay ref={BanRef} beforeChange={bannerChange} >
            {
              topBanners.map((item,index) =>{
                return (
                  <div className="banner-item" key={item.imageUrl}>
                    {/* <a href="todo"> */}
                      <img className="image" src={item.imageUrl} alt={item.typeTitle} />
                    {/* </a> */}
                  </div>
                )
              })
            }
          </Carousel>
        </BannerLeft>
        <BannerRight>  </BannerRight>
        <BannerControl>
          <button className="btn left" onClick={e => BanRef.current.prev()}></button>
          <button className="btn right" onClick={e => BanRef.current.next()}></button>
        </BannerControl>
      </div>
      
    </BannerWrapper>
  )
})

import React ,{memo}from 'react';

import HotRecommend from './c-cpns/hot-recommend';
import NewAlbum from './c-cpns/new-album';
import RecommendRanking from './c-cpns/recommend-ranking';
import RecommendTopBanner from './c-cpns/top-banner';
import { 
  RecommendWrapper ,
  Content,
  RecommendLeft,
  RecommendRight
} from './style';

function Recommend(props) {

  return (
    <RecommendWrapper>
      <RecommendTopBanner/>
      <Content className="wrap-v2">
        <RecommendLeft>
          <HotRecommend/>
          <NewAlbum/>
          <RecommendRanking/>
        </RecommendLeft>
        <RecommendRight></RecommendRight>
      </Content>
    </RecommendWrapper>
  );
}


export default  memo(Recommend);



// ****************************************
// function Recommend(props) {

//   const {getBanners,topBanners} = props;
//   useEffect(() => {
//     getBanners();
//   }, [getBanners])
  
//   return (
//     <div>
//       <h2>Recommend:{topBanners.length}</h2>
//     </div>
//   );
// }

// const mapStateToProps = state => ({
//   topBanners:state.recommend.topBanners
// });

// const mpaDispatchToProps = dispatch => ({
//   getBanners:() => {
//     dispatch(getTopBannerAction())
//   }
// })


// export default connect(mapStateToProps,mpaDispatchToProps) (memo(Recommend));

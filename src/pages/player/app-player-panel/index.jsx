import React, { memo } from 'react'
import LyricContent from './c-cpns/lyric-panel';
import PanelHead from './c-cpns/panel-header'
import PanelList from './c-cpns/panel-list';

export default memo(function AppPlayerPanel() {
  return (
    <div>
      <PanelHead/>
      <PanelList/>
      <LyricContent/>
    </div>
  )
})

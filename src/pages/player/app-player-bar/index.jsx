import React, { memo ,useState, useEffect,useRef,useCallback} from 'react'
import { useDispatch, useSelector } from 'react-redux';

import {getSizeImage,formatDate,getPlaySong} from '@/utils/format-utils';

import { message } from 'antd';

import { Slider } from 'antd';
import {
  PlaybarWrapper,
  PlayInfo,
  Control,
  Operator
} from './style';
import { getSongDetailAction,
         changeSequenceAction,
         changeCurrentSong,
         changeCurrentLyricIndex
 } from '../store/actionCreators';
import AppPlayerPanel from '../app-player-panel';



export default memo(function AppPlayerBar() {

  const [currentTime, setCurrentTime] = useState(0)
  const [progress, setProgress] = useState(0)
  const [isChange, setIsChange] = useState(false)
  const [isPlay, setIsPlay] = useState(false)
  const [showPanel, setShowPanel] = useState(false)

  const { currentSong, sequence, lyricList , currentLyricIndex,playList} = useSelector(state => ({
    currentSong:state.getIn(["player","currentSong"]),
    sequence:state.getIn(["player","sequence"]),
    lyricList:state.getIn(["player","lyricList"]),
    currentLyricIndex:state.getIn(["player","currentLyricIndex"]),
    playList: state.getIn(["player", "playList"]),
  }))
  

  const dispatch =useDispatch();

  const audioRef = useRef();
  useEffect(() => {
    dispatch(getSongDetailAction(1901371647))
  }, [dispatch]);

  useEffect(() => {
    audioRef.current.src = getPlaySong(currentSong.id);
    audioRef.current.play().then(res=>{
      setIsPlay(true);
    }).catch(err =>{
      setIsPlay(false);
    })
  }, [currentSong])

  const picUrl = (currentSong.al&&currentSong.al.picUrl )||"";
  const singerName = (currentSong.ar&&currentSong.ar[0].name)||"未知歌手";
  const duration = currentSong.dt||0;
  const showDuration = formatDate(duration,"mm:ss");
  const showCurrentTime = formatDate(currentTime,"mm:ss");
  // const progress = currentTime / duration *100;

//播放
  const playMusic = useCallback(() =>{
    setIsPlay(!isPlay);
    isPlay ? audioRef.current.pause():audioRef.current.play();
  },[isPlay])

  //进度条时间
  const timeUpdate = (e) =>{
    const currentTime = e.target.currentTime; //定义局部变量
    if(!isChange) {
      setCurrentTime(currentTime*1000);
      setProgress(currentTime*1000 / duration *100);
    }

    
    //绑定歌词
    let i = 1;
    for( i=0;i<lyricList.length;i++){
      let lyricItem = lyricList[i];
      if(currentTime*1000<lyricItem.time){
        break;
      }
    }
    if(currentLyricIndex !== (i-1)){
      dispatch(changeCurrentLyricIndex(i-1));
      const content = lyricList[i - 1] && lyricList[i - 1].content
      console.log(content);
      message.open({
        key: "lyric",
        content: content,
        duration: 0,
        className: "lyric-class"
      })
    } 
    
      
  }

  const handleMusicEnded=()=>{
    if (sequence === 2|| playList.length === 1) { // 单曲循环
      audioRef.current.currentTime = 0;
      audioRef.current.play();
    } else {
      dispatch(changeCurrentSong(1));
    }
  }
  
  const changeSequence =() =>{
    let currentSequence = sequence + 1;
    if(currentSequence>2){
      currentSequence = 0;
    }
    dispatch(changeSequenceAction(currentSequence));
  }
  const changeMusic =(tag)=>{
    dispatch(changeCurrentSong(tag));
  }

  //
  const sliderChange = useCallback((value) => {
    setIsChange(true)
    const currentTime = value / 100 * duration;
    setCurrentTime(currentTime);
    setProgress(value)
    },[duration])

  const afterSliderChange = useCallback((value) => {
    const currentTime = value / 100 * duration /1000 ;
    audioRef.current.currentTime = currentTime;
    setCurrentTime(currentTime*1000);
    setIsChange(false)

    if(!isPlay){
      playMusic();
    }
    },[duration,isPlay,playMusic])

  return (
    <PlaybarWrapper className='sprite_player'>
      <div className='content wrap-v2'>
        <Control isPlaying={isPlay} >
          <button className='sprite_player prev' onClick={e => changeMusic(-1)}></button>
          <button className='sprite_player play' onClick={e => playMusic()}></button>
          <button className='sprite_player next' onClick={e => changeMusic(+1)}></button>
        </Control>
        <PlayInfo>
          <div className='image'>
            <a href='#/'>
            <img src={getSizeImage(picUrl,35) } alt='todo'/>
            </a>
          </div>
          <div className='info'>
            <div className='song'>
              <a href='todo' className='song-name'>{currentSong.name}</a>
              <a href='#/' className='singer-name'>{singerName}</a>
            </div>
            <div className='progress'>
            <Slider defaultValue={30}
                    value={progress}
                    onChange={sliderChange}
                    onAfterChange={afterSliderChange}/>
              <div className="time">
                <span className="now-time">{showCurrentTime}</span>
                <span className="divider">/</span>
                <span className="duration">{showDuration}</span>
              </div>
            </div>
          </div>
        </PlayInfo>
        <Operator sequence={sequence}>
          <div className='left'>
            <button className='sprite_player btn favor'></button>
            <button className='sprite_player btn share'></button>
          </div>
          <div className='right sprite_player'>
            <button className='sprite_player btn volume'></button>
            <button className='sprite_player btn loop' onClick={e => changeSequence()}></button>
            <button className='sprite_player btn playlist' onClick={e => setShowPanel(!showPanel)}>{playList.length}</button>
          </div>
        </Operator>
      </div>
      <audio ref={audioRef} onTimeUpdate={timeUpdate} onEnded={handleMusicEnded}/>
      {showPanel && <AppPlayerPanel />}
    </PlaybarWrapper>
  )
})

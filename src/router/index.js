import React, { memo } from 'react'
import { Navigate, useRoutes } from "react-router-dom";

import HYDiscover from "../pages/discover";
import Recommend from "../pages/discover/c-pages/recommend";
import Ranking from "../pages/discover/c-pages/ranking";
import Songs from "../pages/discover/c-pages/songs";
import Djradio from "../pages/discover/c-pages/djradio";
import Artist from "../pages/discover/c-pages/artist";
import Album from "../pages/discover/c-pages/album";
import Player from "../pages/player";
import HYFriend from "../pages/friend";
import HYMine from "../pages/mine";


function App() {
  let element = useRoutes([
    {
      path:"/",
      element:<Navigate to="/discover/recommend"/> 
    },
    {
      path:"discover",
      element:<Navigate to="/discover/recommend"/> 
    },
    {
      path: "discover",
      element: <HYDiscover />,
      children: [
        {
          path: "recommend",
          element: <Recommend />
        },
        {
          path: "ranking",
          element: <Ranking />
        },
        {
          path: "songs",
          element: <Songs />
        },
        {
          path: "djradio",
          element: <Djradio />
        },
        {
          path: "artist",
          element: <Artist />
        },
        {
          path: "album",
          element: <Album />
        },
        {
          path: "player",
          element: <Player />
        }
      ]
    },
    {
      path: "mine",
      element: <HYMine/>
    },
    {
      path: "friend",
      element: <HYFriend/>
    },
  ]);

  return element;
}

export default memo(function AppPlayerBar() {
  return (
    <div>
      <App/>
    </div>
  )
})
